﻿using DatabaseManagementModule.Models;
using DatabaseManagementModule.Services;
using DatabaseManagementModule.Validation;
using ElasticSearchNestConnector;
using Nest;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using ReportModule;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule;

namespace DatabaseManagementModule
{
    public class DatabaseManagementController : AppController
    {

        public async Task<ResultItem<GetMsColumnsResult>> GetColumns(GetColumnsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetMsColumnsResult>>(async () =>
           {
               var result = new ResultItem<GetMsColumnsResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new GetMsColumnsResult()
               };

               var serviceAgentElastic = new ElasticServiceAgent(Globals);

               var getDbItemModel = await serviceAgentElastic.GetMsSQLDbItemModel(request);

               result.ResultState = getDbItemModel.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               if (!getDbItemModel.Result.Any())
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Db-Item with Db-Schema found!";
                   return result;
               }
               var dbItemModel = getDbItemModel.Result.First();

               var connectionResult = await serviceAgentElastic.GetDatabaseConnectionModel(request);

               result.ResultState = connectionResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               result.ResultState = ValidationController.ValidationDatabaseConnectionModel(connectionResult.Result, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               if (connectionResult.Result.DatabasesOnServers.Count != 1)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Only exact one database-connection is allowed!";
                   return result;
               }

               var connectionInfos = await connectionResult.Result.GetConnectionStringsMsSql(Globals, request.MasterPassword);

               result.ResultState = connectionInfos.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               var connection = connectionInfos.Result.First();

               var msSQLConnector = new MsSQLConnector(Globals);

               var getColumnsResult = await msSQLConnector.GetMsColumns(dbItemModel, connection.Additional1);
               result.ResultState = getColumnsResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               var dataTypes = getColumnsResult.Result.ColumnRawItems.Select(col => col.DataType).ToList();

               var checkResult = await serviceAgentElastic.CheckObjects(dataTypes, Config.LocalData.Class_DataTypes__Ms_SQL_.GUID);

               result.ResultState = checkResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               var getDbColumnsResult = await serviceAgentElastic.GetColumnsOfDbItem(new List<string> { request.IdTableOrViewOrFunction });

               result.ResultState = getDbColumnsResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               var columnsToCheck = (from col in getColumnsResult.Result.ColumnRawItems
                                     join colDb in getDbColumnsResult.Result on col.Column.Name equals colDb.Name_Object into colsDb
                                     from colDb in colsDb.DefaultIfEmpty()
                                     select new { col, colDb }).ToList();

               var columnsToDelete = (from colDb in getDbColumnsResult.Result
                                      join col in getColumnsResult.Result.ColumnRawItems on colDb.Name_Object equals col.Column.Name into cols
                                      from col in cols.DefaultIfEmpty()
                                      where col == null
                                      select colDb).ToList();

               foreach (var col in columnsToCheck)
               {
                   if (col.colDb == null)
                   {
                       col.col.Column.GUID = Globals.NewGUID;
                       col.col.Column.New_Item = true;
                   }
                   else
                   {
                       col.col.Column.GUID = col.colDb.ID_Object;
                       col.col.Column.New_Item = false;
                   }
               }

               var relationConfig = new clsRelationConfig(Globals);
               var columnsToSave = columnsToCheck.Where(col => col.col.Column.New_Item.Value == true).Select(col => col).ToList();
               long orderId = 1;
               var relationsToSave = columnsToSave.Select(col => relationConfig.Rel_ObjectRelation(col.col.Column, dbItemModel.DbItem, Config.LocalData.RelationType_belongs_to, orderId: orderId++)).ToList();
               relationsToSave.AddRange(from column in columnsToSave
                                        join dbDataType in checkResult.Result on column.col.DataType.Name equals dbDataType.Name
                                        select relationConfig.Rel_ObjectRelation(dbDataType, column.col.Column, Config.LocalData.RelationType_belongs_to));

               var attributesToSave = columnsToSave.Select(col => relationConfig.Rel_ObjectAttribute(col.col.Column, Config.LocalData.AttributeType_Length, col.col.MaxLength)).ToList();

               result.ResultState = await serviceAgentElastic.SaveObjects(columnsToSave.Select(col => col.col.Column).ToList());
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }
               result.ResultState = await serviceAgentElastic.SaveRelations(relationsToSave);
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }
               result.ResultState = await serviceAgentElastic.SaveAttributes(attributesToSave);
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }




               return result;
           });

            return taskResult;
        }

        public async Task<clsOntologyItem> CheckExistanceDBItems(CheckExistanceDBItemsRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = Globals.LState_Success.Clone();

                request.MessageOutput?.OutputInfo("Validate request...");

                result = ValidationController.ValidateCheckExistanceDBItemsRequest(request, Globals);

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                request.MessageOutput?.OutputInfo("Get Model...");

                var elasticAgent = new ElasticServiceAgent(Globals);

                var serviceResult = await elasticAgent.GetCheckExistanceDBItemsModel(request);

                result = serviceResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Model.");

                var objectsToCheck = serviceResult.Result.DbItems;
                if (serviceResult.Result.GetObjectsFromReportConfig != null)
                {
                    var reportController = new ReportController(Globals, false);
                    var getObjectsRequest = new GetObjectsFormReportRequest(serviceResult.Result.GetObjectsFromReportConfig.GUID, request.CancellationToken);
                    var getObjectsResult = await reportController.GetObjectsFromReport(getObjectsRequest);
                    objectsToCheck.AddRange(getObjectsResult.Result.ValidObjects);
                }

                if (!objectsToCheck.Any())
                {
                    result.Additional1 = "No Items!";
                    request.MessageOutput?.OutputWarning(result.Additional1);
                    return result;
                }

                var checkExistanceByListRequest = new CheckExistanceDBItemsWithListRequest(objectsToCheck, request.MasterPassword, request.CancellationToken);
                result = await CheckExistanceDBItems(checkExistanceByListRequest);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<DBItemsWithListModel>> GetDBItems(List<clsOntologyItem> dbItems, string masterPassword, bool queryDbItems, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<DBItemsWithListModel>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new DBItemsWithListModel()
                };

                if (!dbItems.Any())
                {
                    return result;
                }

                var elasticAgent = new ElasticServiceAgent(Globals);
                if (queryDbItems)
                {
                    messageOutput?.OutputInfo("Query DB-Items...");
                    var oItemsResult = await elasticAgent.GetOItems(dbItems.Select(dbItem => new clsOntologyItem { GUID = dbItem.GUID, Type = Globals.Type_Object }).ToList());
                    result.ResultState = oItemsResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the DB-Items!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    dbItems = oItemsResult.Result;
                    messageOutput?.OutputInfo("Habe DB-Items.");
                }

                messageOutput?.OutputInfo("Check for non DB-Items...");

                var nonDbItems = dbItems.Where(item => item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Columns.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Jobs.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Linked_Servers.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Routines.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Synonyms.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Tables.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Triggers.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Views.GUID).ToList();
                if (nonDbItems.Any())
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The requested items are not all DB-Items:";
                    nonDbItems.ForEach(item =>
                    {
                        messageOutput?.OutputError(item.Name);
                        item.Mark = true;
                        item.Additional1 = "Item not existing!";
                    });
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo("Checked for non DB-Items.");

                var getModelResult = await elasticAgent.GetDBItemsWithListModel(dbItems);

                result.ResultState = getModelResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var invalidDbItems = (from dbItem in getModelResult.Result.DbItems
                                      join dbOnServer in getModelResult.Result.DbOnServersToDBItems on dbItem.GUID equals dbOnServer.ID_Other into dbOnServers
                                      from dbOnServer in dbOnServers.DefaultIfEmpty()
                                      join dbSynonymDbOnServer in getModelResult.Result.DBSynonymsToDBOnServers on dbItem.GUID equals dbSynonymDbOnServer.ID_Object into dbSynonymsDbOnServer
                                      from dbSynonymDbOnServer in dbSynonymsDbOnServer.DefaultIfEmpty()
                                      where dbOnServer == null && dbSynonymDbOnServer == null
                                      select dbItem).ToList();
                invalidDbItems.ForEach(invalidDbItem =>
                {
                    invalidDbItem.Additional1 = "Item is invalid!";
                    invalidDbItem.Mark = true;
                });
                messageOutput?.OutputInfo($"{invalidDbItems.Count} invalid items.");

                var validDbItems = (from dbItem in getModelResult.Result.DbItems
                                    join invalidCheckItem in invalidDbItems on dbItem equals invalidCheckItem into invalids
                                    from invlidCheckItem in invalids.DefaultIfEmpty()
                                    where invlidCheckItem == null
                                    select dbItem).ToList();

                validDbItems.ForEach(item =>
                {
                    item.Mark = false;
                });

                messageOutput?.OutputInfo($"{validDbItems.Count} valid items.");

                if (validDbItems.Count == 0)
                {
                    messageOutput?.OutputInfo($"finished.");
                    return result;
                }

                messageOutput?.OutputInfo($"Get Database On Servers...");
                var databaseOnServers = getModelResult.Result.DbOnServersToDBItems.GroupBy(rel => new { rel.ID_Object, rel.Name_Object, rel.ID_Parent_Object }).Select(rel => new clsOntologyItem
                {
                    GUID = rel.Key.ID_Object,
                    Name = rel.Key.Name_Object,
                    GUID_Parent = rel.Key.ID_Parent_Object,
                    Type = Globals.Type_Object
                }).ToList();

                var dbOnServersToAdd = (from dbOnServer in getModelResult.Result.DBSynonymsToDBOnServers.GroupBy(rel => new { rel.ID_Other, rel.Name_Other, rel.ID_Parent_Other }).Select(rel => new clsOntologyItem
                {
                    GUID = rel.Key.ID_Other,
                    Name = rel.Key.Name_Other,
                    GUID_Parent = rel.Key.ID_Parent_Other,
                    Type = Globals.Type_Object
                })
                                        join dbOnServerInList in databaseOnServers on dbOnServer.GUID equals dbOnServerInList.GUID into dbOnServerInLists
                                        from dbOnServerInList in dbOnServerInLists.DefaultIfEmpty()
                                        where dbOnServerInList == null
                                        select dbOnServer).ToList();


                databaseOnServers.AddRange(dbOnServersToAdd);

                var getDatabaseConnectionRequest = new GetDatabaseConnectionModelRequest(databaseOnServers);
                var connectionResult = await elasticAgent.GetDatabaseConnectionModel(getDatabaseConnectionRequest);

                result.ResultState = connectionResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo($"Have Database On Servers.");

                result.ResultState = ValidationController.ValidationDatabaseConnectionModel(connectionResult.Result, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo($"Get Connection infos...");
                var connectionInfos = await connectionResult.Result.GetConnectionStringsMsSql(Globals, masterPassword);

                foreach (var validDbItem in validDbItems)
                {
                    validDbItem.OList_Rel = new List<clsOntologyItem>();
                    getModelResult.Result.ValidDBItemsWithConnectionInfo.Add(validDbItem);
                    if (validDbItem.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Synonyms.GUID)
                    {

                        validDbItem.OList_Rel.AddRange (from dbOnServerRel in getModelResult.Result.DbOnServersToDBItems.Where(rel => rel.ID_Other == validDbItem.GUID)
                                               join connInfo in connectionInfos.Result on dbOnServerRel.ID_Object equals connInfo.GUID
                                               select connInfo);
                    }
                    else
                    {
                        validDbItem.OList_Rel.AddRange(from dbOnServerRel in getModelResult.Result.DBSynonymsToDBOnServers.Where(rel => rel.ID_Object == validDbItem.GUID)
                                               join connInfo in connectionInfos.Result on dbOnServerRel.ID_Other equals connInfo.GUID
                                               select connInfo);
                    }
                }

                messageOutput?.OutputInfo($"Have Connection infos.");

                result = getModelResult;

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> CheckExistanceDBItems(CheckExistanceDBItemsWithListRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var getDBItemsResult = await GetDBItems(request.DBItems, request.MasterPassword, request.QueryDBItems, request.MessageOutput);
                result = getDBItemsResult.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var elasticAgent = new ElasticServiceAgent(Globals);
                var relationConfig = new clsRelationConfig(Globals);
                var msSQLAgent = new MsSQLConnector(Globals);

                var objectsToSave = new List<clsOntologyItem>();
                var relationsToSave = new List<clsObjectRel>();
                var attributeToSave = new List<clsObjectAtt>();

                foreach (var validDbItem in getDBItemsResult.Result.ValidDBItemsWithConnectionInfo)
                {

                    if (validDbItem.OList_Rel.Any())
                    {
                        foreach (var connInfo in validDbItem.OList_Rel)
                        {
                            var foundResult = await msSQLAgent.CheckExistance(validDbItem, connInfo.Additional1);

                            var resultItem = new clsOntologyItem
                            {
                                GUID = Globals.NewGUID,
                                Name = validDbItem.Name,
                                GUID_Parent = CheckItemExistance.Config.LocalData.Class_Check_Result__DBItem_Existance_.GUID,
                                Type = Globals.Type_Object
                            };

                            objectsToSave.Add(resultItem);
                            relationsToSave.Add(relationConfig.Rel_ObjectRelation(resultItem, foundResult, CheckItemExistance.Config.LocalData.RelationType_result));
                            if (foundResult.GUID == Globals.LState_Error.GUID)
                            {
                                attributeToSave.Add(relationConfig.Rel_ObjectAttribute(resultItem, CheckItemExistance.Config.LocalData.AttributeType_Message, foundResult.Additional1));
                            }

                            attributeToSave.Add(relationConfig.Rel_ObjectAttribute(resultItem, CheckItemExistance.Config.LocalData.AttributeType_DateTimestamp, DateTime.Now));
                            relationsToSave.Add(relationConfig.Rel_ObjectRelation(resultItem, validDbItem, CheckItemExistance.Config.LocalData.RelationType_belonging_Resource));
                            relationsToSave.Add(relationConfig.Rel_ObjectRelation(resultItem, connInfo, CheckItemExistance.Config.LocalData.RelationType_belonging));
                        }
                    }
                    else
                    {
                        var resultItem = new clsOntologyItem
                        {
                            GUID = Globals.NewGUID,
                            Name = validDbItem.Name,
                            GUID_Parent = CheckItemExistance.Config.LocalData.Class_Check_Result__DBItem_Existance_.GUID,
                            Type = Globals.Type_Object
                        };

                        objectsToSave.Add(resultItem);
                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(resultItem, CheckItemExistance.Config.LocalData.Object_Error, CheckItemExistance.Config.LocalData.RelationType_result));
                        attributeToSave.Add(relationConfig.Rel_ObjectAttribute(resultItem, CheckItemExistance.Config.LocalData.AttributeType_DateTimestamp, DateTime.Now));
                        attributeToSave.Add(relationConfig.Rel_ObjectAttribute(resultItem, CheckItemExistance.Config.LocalData.AttributeType_Message, "No valid Database On Server!"));
                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(resultItem, validDbItem, CheckItemExistance.Config.LocalData.RelationType_belonging_Resource));
                    }

                    if (objectsToSave.Count > 500)
                    {
                        request.MessageOutput?.OutputInfo($"Saving {objectsToSave.Count} Objects, {attributeToSave.Count} Attributes, {relationsToSave.Count} Relations.");
                        result = await elasticAgent.SaveObjects(objectsToSave);
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            result.Additional1 = "Error while saving the Result-Items!";
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        result = await elasticAgent.SaveAttributes(attributeToSave);
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            result.Additional1 = "Error while saving the Attributes of Result-Items!";
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        result = await elasticAgent.SaveRelations(relationsToSave);
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            result.Additional1 = "Error while saving the Relations of Result-Items!";
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        objectsToSave.Clear();
                        attributeToSave.Clear();
                        relationsToSave.Clear();
                    }
                }

                if (objectsToSave.Any())
                {
                    request.MessageOutput?.OutputInfo($"Saving {objectsToSave.Count} Objects, {attributeToSave.Count} Attributes, {relationsToSave.Count} Relations.");
                    result = await elasticAgent.SaveObjects(objectsToSave);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Result-Items!";
                        request.MessageOutput?.OutputError(result.Additional1);
                        return result;
                    }

                    result = await elasticAgent.SaveAttributes(attributeToSave);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Attributes of Result-Items!";
                        request.MessageOutput?.OutputError(result.Additional1);
                        return result;
                    }

                    result = await elasticAgent.SaveRelations(relationsToSave);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Relations of Result-Items!";
                        request.MessageOutput?.OutputError(result.Additional1);
                        return result;
                    }
                }

                request.MessageOutput?.OutputInfo($"Finished.");

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> CheckMSConnectionString(CheckConnectionStringRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var elasticAgent = new ElasticServiceAgent(Globals);

                request.MessageOutput?.OutputInfo("Validate request...");

                result = ValidationController.ValidateCheckMSConnectionString(request, Globals);
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                var searchDatabaseConnection = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConnectionString
                    }
                };

                request.MessageOutput?.OutputInfo("Get Database-Connection...");
                var serviceResult = await elasticAgent.GetObjects(searchDatabaseConnection);

                result = serviceResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while getting the database-connection-object!";
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                var databaseConnection = serviceResult.Result.FirstOrDefault();

                if (databaseConnection == null)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "No object found!";
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Have Database-Connection: { databaseConnection.Name }");

                try
                {
                    request.MessageOutput?.OutputInfo("Try connecting...");
                    var connection = new System.Data.SqlClient.SqlConnection(databaseConnection.Name);
                    connection.Open();
                    request.MessageOutput?.OutputInfo("Connected.");
                    connection.Close();
                    request.MessageOutput?.OutputInfo("Connection closed.");

                }
                catch (Exception ex)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ExportCodeToFileResult>> ExportCodeToFile(ExportCodeToFileRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<ExportCodeToFileResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ExportCodeToFileResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState = ValidationController.ValidateExportCodeToFileRequest(request, Globals);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");

                var elasticAgent = new ElasticServiceAgent(Globals);

                request.MessageOutput?.OutputInfo("Get model...");

                var modelResult = await elasticAgent.GetExportCodeToFileModel(request);

                result.ResultState = modelResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have model.");



                var textParserController = new TextParserController(Globals);

                request.MessageOutput?.OutputInfo("Get Textparser...");
                var textParserResult = await textParserController.GetTextParser(new clsOntologyItem
                {
                    GUID = modelResult.Result.ConfigToTextparser.ID_Other,
                    Name = modelResult.Result.ConfigToTextparser.Name_Other,
                    GUID_Parent = modelResult.Result.ConfigToTextparser.ID_Parent_Other,
                    Type = modelResult.Result.ConfigToTextparser.Ontology
                });

                result.ResultState = textParserResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var textParser = textParserResult.Result.FirstOrDefault();

                result.ResultState = ValidationController.ValidateExportToFileTextparser(textParser, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Textparser.");

                request.MessageOutput?.OutputInfo("Get Textparser-Fields...");
                var textParserFieldsResult = await textParserController.GetParserFields(new clsOntologyItem
                {
                    GUID = textParser.IdFieldExtractor,
                    Name = textParser.NameFieldExtractor,
                    GUID_Parent = textParser.IdClassFieldExtractor,
                    Type = Globals.Type_Object
                });

                result.ResultState = textParserFieldsResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var textParserFields = textParserFieldsResult.Result;

                result.ResultState = ValidationController.ValidateExportToFileTextparserFields(textParserFields, modelResult.Result, Globals);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Textparser-Fields.");

                var dbReaderFiles = new clsUserAppDBSelector(textParser.NameServer, textParser.Port, textParser.NameIndexElasticSearch, Globals.SearchRange, Globals.Session);

                var sortFields = new List<SortField>();

                sortFields.Add(new SortField
                {
                    Field = modelResult.Result.ConfigToFieldServer.Name_Other,
                    Order = SortOrder.Ascending
                });
                sortFields.Add(new SortField
                {
                    Field = modelResult.Result.ConfigToFieldDatabase.Name_Other,
                    Order = SortOrder.Ascending
                });
                sortFields.Add(new SortField
                {
                    Field = modelResult.Result.ConfigToFieldSchema.Name_Other,
                    Order = SortOrder.Ascending
                });
                sortFields.Add(new SortField
                {
                    Field = modelResult.Result.ConfigToFieldRoutineType.Name_Other,
                    Order = SortOrder.Ascending
                });
                sortFields.Add(new SortField
                {
                    Field = modelResult.Result.ConfigToFieldRoutine.Name_Other,
                    Order = SortOrder.Ascending
                });
                var filesDocs = dbReaderFiles.GetData_Documents(-1, textParser.NameIndexElasticSearch, textParser.NameEsType, modelResult.Result.Pattern.Val_String);

                var filesFilter = new List<ExportCodeToFileFilter>();
                var documents = filesDocs.Documents.Where(doc => doc.Dict.ContainsKey(modelResult.Result.ConfigToFieldDatabase.Name_Other) &&
                                                                 doc.Dict.ContainsKey(modelResult.Result.ConfigToFieldLine.Name_Other) &&
                                                                 doc.Dict.ContainsKey(modelResult.Result.ConfigToFieldRoutine.Name_Other) &&
                                                                 doc.Dict.ContainsKey(modelResult.Result.ConfigToFieldRoutineType.Name_Other) &&
                                                                 doc.Dict.ContainsKey(modelResult.Result.ConfigToFieldSchema.Name_Other) &&
                                                                 doc.Dict.ContainsKey(modelResult.Result.ConfigToFieldServer.Name_Other)).ToList();

                var rootPath = modelResult.Result.ConfigToPath.Name_Other;
                var sortFieldsExport = new List<SortField>
                {
                        new SortField
                        {
                            Field = modelResult.Result.ConfigToFieldLine.Name_Other,
                            Order = SortOrder.Ascending
                        }
                };
                foreach (var fileDoc in filesDocs.Documents)
                {
                    var server = fileDoc.Dict[modelResult.Result.ConfigToFieldServer.Name_Other].ToString();
                    var database = fileDoc.Dict[modelResult.Result.ConfigToFieldDatabase.Name_Other].ToString();
                    var schema = fileDoc.Dict[modelResult.Result.ConfigToFieldSchema.Name_Other].ToString();
                    var routine = fileDoc.Dict[modelResult.Result.ConfigToFieldRoutine.Name_Other].ToString();
                    var routineType = fileDoc.Dict[modelResult.Result.ConfigToFieldRoutineType.Name_Other].ToString();

                    var fileName = Path.Combine(rootPath, server, $"{database}_{schema}_{routineType}_{routine}") + ".sql";

                    if (!filesFilter.Any(file => file.FileName.ToLower() == fileName.ToLower()))
                    {
                        var fileFilterItem = new ExportCodeToFileFilter
                        {
                            Database = database,
                            EsIndex = textParser.NameIndexElasticSearch,
                            EsPort = textParser.Port,
                            EsServer = textParser.NameServer,
                            EsType = textParser.NameEsType,
                            FileName = fileName,
                            Model = modelResult.Result,
                            Routine = routine,
                            RoutineType = routineType,
                            Schema = schema,
                            Server = server,
                            SortFields = sortFieldsExport
                        };
                        var exportResult = ExportCodeToFile(fileFilterItem);
                        filesFilter.Add(fileFilterItem);

                    }
                }

                return result;
            });

            return taskResult;
        }

        public clsOntologyItem ExportCodeToFile(ExportCodeToFileFilter filter)
        {
            var result = Globals.LState_Success.Clone();

            var dbReaderExport = new clsUserAppDBSelector(filter.EsServer, filter.EsPort, filter.EsIndex, Globals.SearchRange, Globals.Session);
            var count = -1;
            var page = 0;
            string scrollId = null;
            try
            {
                var path = Path.GetDirectoryName(filter.FileName);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                using (var textWriter = new StreamWriter(filter.FileName))
                {
                    while (count != 0)
                    {
                        var documents = dbReaderExport.GetData_Documents(5000, filter.EsIndex, filter.EsType, filter.GetQuery(), page, scrollId, filter.SortFields);
                        count = documents.Documents.Count;
                        page++;
                        scrollId = documents.ScrollId;
                        foreach (var document in documents.Documents)
                        {
                            var text = "";
                            if (document.Dict.ContainsKey(filter.Model.ConfigToFieldText.Name_Other))
                            {
                                text = document.Dict[filter.Model.ConfigToFieldText.Name_Other].ToString();
                            }

                            textWriter.Write(text);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
            }


            return result;
        }

        public async Task<clsOntologyItem> GetMSSqlCode(GetMSSqlCodeRequest request)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = Globals.LState_Success.Clone();

                request.MessageOutput?.OutputInfo("Validate request...");

                result = ValidationController.ValidateGetMSSqlCodeRequest(request, Globals);

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                request.MessageOutput?.OutputInfo("Get Model...");
                var elasticAgent = new ElasticServiceAgent(Globals);

                var getModelResult = await elasticAgent.GetMsSQLCodeModel(request);

                result = getModelResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Model.");

                if (!getModelResult.Result.DbItems.Any())
                {
                    request.MessageOutput?.OutputInfo("No DB-Items!");
                    return result;
                }

                request.MessageOutput?.OutputInfo("Get Db-Items...");
                var getDbItemsResult = await GetDBItems(getModelResult.Result.DbItems, request.MasterPassword, false, request.MessageOutput);

                result = getDbItemsResult.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Db-Items.");

                var relationConfig = new clsRelationConfig(Globals);
                var msSQLAgent = new MsSQLConnector(Globals);

                var objectsToSave = new List<clsOntologyItem>();
                var relationsToSave = new List<clsObjectRel>();
                var attributeToSave = new List<clsObjectAtt>();

                var sessionDate = DateTime.Now;

                foreach (var invalidDbItem in getDbItemsResult.Result.DbItems.Where(dbItem => dbItem.Mark.Value == true))
                {
                    var sessionItem = new clsOntologyItem
                    {
                        GUID = Globals.NewGUID,
                        Name = Globals.GetValidOItemName($"{sessionDate} - {invalidDbItem.Name}"),
                        GUID_Parent = GetCodeOfDBItem.Config.LocalData.Class_Session_State__Get_Code_of_DB_Item_.GUID,
                        Type = Globals.Type_Object
                    };
                    objectsToSave.Add(sessionItem);
                    attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                        sessionItem,
                        GetCodeOfDBItem.Config.LocalData.AttributeType_Datetimestamp__Create_,
                        sessionDate));
                    attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                        sessionItem,
                        GetCodeOfDBItem.Config.LocalData.AttributeType_Succeeded,
                        false));
                    attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                        sessionItem,
                        GetCodeOfDBItem.Config.LocalData.AttributeType_Message,
                        "Db-Item is invalid"));
                    relationsToSave.Add(relationConfig.Rel_ObjectRelation(
                        getModelResult.Result.Config,
                        sessionItem,
                        GetCodeOfDBItem.Config.LocalData.RelationType_contains));
                    relationsToSave.Add(relationConfig.Rel_ObjectRelation(
                        sessionItem,
                        invalidDbItem,
                        GetCodeOfDBItem.Config.LocalData.RelationType_DB_Item));

                }
                foreach (var validDbItem in (from validDbItem in getDbItemsResult.Result.ValidDBItemsWithConnectionInfo
                                             join schemaItem in getDbItemsResult.Result.DbItemsToShemaItem.Select(rel => new clsOntologyItem
                                             {
                                                 GUID = rel.ID_Other,
                                                 Name = rel.Name_Other,
                                                 GUID_Parent = rel.ID_Parent_Other,
                                                 Type = rel.Ontology,
                                                 GUID_Related = rel.ID_Object
                                             }).ToList() on validDbItem.GUID equals schemaItem.GUID_Related into schemaItems
                                             from schemaItem in schemaItems.DefaultIfEmpty()
                                             select new { validDbItem, schemaItem }).ToList())
                {
                    var sessionItem = new clsOntologyItem
                    {
                        GUID = Globals.NewGUID,
                        Name = Globals.GetValidOItemName($"{sessionDate} - {validDbItem.validDbItem.Name}"),
                        GUID_Parent = GetCodeOfDBItem.Config.LocalData.Class_Session_State__Get_Code_of_DB_Item_.GUID,
                        Type = Globals.Type_Object
                    };

                    objectsToSave.Add(sessionItem);
                    attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                        sessionItem,
                        GetCodeOfDBItem.Config.LocalData.AttributeType_Datetimestamp__Create_,
                        sessionDate));
                    relationsToSave.Add(relationConfig.Rel_ObjectRelation(
                        getModelResult.Result.Config,
                        sessionItem,
                        GetCodeOfDBItem.Config.LocalData.RelationType_contains));

                    relationsToSave.Add(relationConfig.Rel_ObjectRelation(
                        sessionItem,
                        validDbItem.validDbItem,
                        GetCodeOfDBItem.Config.LocalData.RelationType_DB_Item));

                    if (validDbItem.schemaItem == null)
                    {
                        attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                            sessionItem,
                            GetCodeOfDBItem.Config.LocalData.AttributeType_Succeeded,
                            false));

                        attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                            sessionItem,
                            GetCodeOfDBItem.Config.LocalData.AttributeType_Message,
                            "No Schema-Item found!"));
                        
                    }
                    else
                    {
                        var dbItemWithSchema = new MsSqlDbItemModel
                        {
                            DbItem = validDbItem.validDbItem,
                            DbSchema = validDbItem.schemaItem
                        };

                        if (validDbItem.validDbItem.OList_Rel.Any())
                        {
                            foreach (var connInfo in validDbItem.validDbItem.OList_Rel)
                            {
                                var codeResult = await msSQLAgent.GetMsCode(dbItemWithSchema, connInfo.Additional1);
                                if (codeResult.ResultState.GUID == Globals.LState_Error.GUID)
                                {
                                    attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                                        sessionItem,
                                        GetCodeOfDBItem.Config.LocalData.AttributeType_Succeeded,
                                        false));

                                    attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                                        sessionItem,
                                        GetCodeOfDBItem.Config.LocalData.AttributeType_Message,
                                        $"{validDbItem.validDbItem.Name}: {codeResult.ResultState.Additional1}!"));
                                }
                                else
                                {
                                    attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                                        sessionItem,
                                        GetCodeOfDBItem.Config.LocalData.AttributeType_Succeeded,
                                        true));


                                    attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                                        sessionItem,
                                        GetCodeOfDBItem.Config.LocalData.AttributeType_Code,
                                        codeResult.Result));
                                }
                            }
                        }
                        else
                        {
                            attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                                sessionItem,
                                GetCodeOfDBItem.Config.LocalData.AttributeType_Succeeded,
                                false));

                            attributeToSave.Add(relationConfig.Rel_ObjectAttribute(
                                sessionItem,
                                GetCodeOfDBItem.Config.LocalData.AttributeType_Message,
                                $"{validDbItem.validDbItem.Name} has no Connection-Information!"));
                        }
                    }
                    
                    if (objectsToSave.Count > 500)
                    {
                        request.MessageOutput?.OutputInfo($"Saving {objectsToSave.Count} Objects, {attributeToSave.Count} Attributes, {relationsToSave.Count} Relations.");
                        result = await elasticAgent.SaveObjects(objectsToSave);
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            result.Additional1 = "Error while saving the Result-Items!";
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        result = await elasticAgent.SaveAttributes(attributeToSave);
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            result.Additional1 = "Error while saving the Attributes of Result-Items!";
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        result = await elasticAgent.SaveRelations(relationsToSave);
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            result.Additional1 = "Error while saving the Relations of Result-Items!";
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        objectsToSave.Clear();
                        attributeToSave.Clear();
                        relationsToSave.Clear();
                    }
                }

                if (objectsToSave.Any())
                {
                    request.MessageOutput?.OutputInfo($"Saving {objectsToSave.Count} Objects, {attributeToSave.Count} Attributes, {relationsToSave.Count} Relations.");
                    result = await elasticAgent.SaveObjects(objectsToSave);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Result-Items!";
                        request.MessageOutput?.OutputError(result.Additional1);
                        return result;
                    }

                    result = await elasticAgent.SaveAttributes(attributeToSave);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Attributes of Result-Items!";
                        request.MessageOutput?.OutputError(result.Additional1);
                        return result;
                    }

                    result = await elasticAgent.SaveRelations(relationsToSave);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Relations of Result-Items!";
                        request.MessageOutput?.OutputError(result.Additional1);
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }


        public DatabaseManagementController(Globals globals) : base(globals)
        {
        }
    }
}
