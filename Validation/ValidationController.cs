﻿using DatabaseManagementModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;

namespace DatabaseManagementModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateGetColumnsRequest(GetColumnsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdTableOrViewOrFunction))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Id of table or view or function is empty!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateTableViewOrFunction(clsOntologyItem dbItem, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (dbItem.GUID_Parent != Config.LocalData.Class_DB_Tables.GUID && 
                dbItem.GUID_Parent != Config.LocalData.Class_DB_Views.GUID &&
                dbItem.GUID_Parent != Config.LocalData.Class_DB_Routines.GUID)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"The Db-Item {dbItem.Name} is no valid table, view or function!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateIdList(List<string> idList, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var invalidIdCount = idList.Count(id => !globals.is_GUID(id));
            if (invalidIdCount > 0)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"{invalidIdCount} Invalid Ids in List!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidationDatabaseConnectionModel(DatabaseConnectionModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(DatabaseConnectionModel.DatabasesOnServers))
            {
                if (!model.DatabasesOnServers.Any())
                {
                    result = globals.LState_Nothing.Clone();
                    result.Additional1 = "No Database on Server found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(DatabaseConnectionModel.DbOnServersToServers))
            {
                if ((from databaseOnServer in model.DatabasesOnServers
                     join server in model.DbOnServersToServers on databaseOnServer.GUID equals server.ID_Object
                     select new { databaseOnServer, server}).Count() != model.DatabasesOnServers.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The number of database on servers is not equal to the number of servers!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(DatabaseConnectionModel.DbOnServersToDatabases))
            {
                if ((from databaseOnServer in model.DatabasesOnServers
                     join database in model.DbOnServersToDatabases on databaseOnServer.GUID equals database.ID_Object
                     select new { databaseOnServer, database }).Count() != model.DatabasesOnServers.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The number of database on servers is not equal to the number of databases!";
                    return result;
                }
            }



            return result;
        }

        public static clsOntologyItem ValidateCheckMSConnectionString(CheckConnectionStringRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConnectionString))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConnection is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConnectionString))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConnection is not valid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateExportCodeToFileRequest(ExportCodeToFileRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is not valid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndPartlySetExportCodeToFileModel(OntologyModDBConnector dbReader, ExportCodeToFileModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty( propertyName) || propertyName == nameof(ExportCodeToFileModel.Config))
            {
                if (model.Config == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No config can be found!";
                    return result;
                }

                if (model.Config.GUID_Parent != ExportRoutine.Config.LocalData.Class_Export_Code_to_File.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"No config is no Object of Class {ExportRoutine.Config.LocalData.Class_Export_Code_to_File.Name}!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportCodeToFileModel.ConfigToFieldDatabase))
            {
                var databaseFieldRels = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_Database_Field.ID_RelationType).ToList();
                if (databaseFieldRels.Count() != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Database-Field or too much Fields provided!";
                    return result;
                }
                model.ConfigToFieldDatabase = databaseFieldRels.First();
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportCodeToFileModel.ConfigToFieldLine))
            {
                var lineFieldRels = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_Line_Field.ID_RelationType).ToList();
                if (lineFieldRels.Count() != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Line-Field or too much Fields provided!";
                    return result;
                }
                model.ConfigToFieldLine = lineFieldRels.First();
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportCodeToFileModel.ConfigToFieldRoutine))
            {
                var routineFieldRels = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_Routine_Field.ID_RelationType).ToList();
                if (routineFieldRels.Count() != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Routine-Field or too much Fields provided!";
                    return result;
                }
                model.ConfigToFieldRoutine = routineFieldRels.First();
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportCodeToFileModel.ConfigToFieldRoutineType))
            {
                var routineTypeFieldRels = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_Routine_Type_Field.ID_RelationType).ToList();
                if (routineTypeFieldRels.Count() != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No RoutineType-Field or too much Fields provided!";
                    return result;
                }
                model.ConfigToFieldRoutineType = routineTypeFieldRels.First();
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportCodeToFileModel.ConfigToFieldSchema))
            {
                var schemaFieldRels = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_Schema_Field.ID_RelationType).ToList();
                if (schemaFieldRels.Count() != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Schema-Field or too much Fields provided!";
                    return result;
                }
                model.ConfigToFieldSchema = schemaFieldRels.First();
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportCodeToFileModel.ConfigToFieldServer))
            {
                var serverFieldRels = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_Server_Field.ID_RelationType).ToList();
                if (serverFieldRels.Count() != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Server-Field or too much Fields provided!";
                    return result;
                }
                model.ConfigToFieldServer = serverFieldRels.First();
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportCodeToFileModel.ConfigToFieldText))
            {
                var textFieldRels = dbReader.ObjectRels.Where(rel => rel.ID_RelationType == ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_Text_Field.ID_RelationType).ToList();
                if (textFieldRels.Count() != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Text-Field or too much Fields provided!";
                    return result;
                }
                model.ConfigToFieldText = textFieldRels.First();
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportCodeToFileModel.ConfigToTextparser))
            {
                
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Textparser or too much Textparsers provided!";
                    return result;
                }
                model.ConfigToTextparser = dbReader.ObjectRels.First();
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportCodeToFileModel.ConfigToPath))
            {

                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Path or too much Paths provided!";
                    return result;
                }
                model.ConfigToPath = dbReader.ObjectRels.First();
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportCodeToFileModel.ConfigToPattern))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Pattern or too much Pattern provided!";
                    return result;
                }
                model.ConfigToPattern = dbReader.ObjectRels.First();
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(ExportCodeToFileModel.Pattern))
            {
                if (dbReader.ObjAtts.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Pattern-Attribute or too much Pattern-Attributes provided!";
                    return result;
                }
                model.Pattern = dbReader.ObjAtts.First();
            }

            return result;
        }

        public static clsOntologyItem ValidateExportToFileTextparserFields(List<ParserField> parserFields, ExportCodeToFileModel model, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var sbFieldNotPresent = new StringBuilder();
            if (!parserFields.Any(field => field.IdField == model.ConfigToFieldDatabase.ID_Other))
            {
                result = globals.LState_Error.Clone();
                sbFieldNotPresent.AppendLine($"Field {model.ConfigToFieldDatabase.Name_Other} is not present in Textparser!");
            }

            if (!parserFields.Any(field => field.IdField == model.ConfigToFieldLine.ID_Other))
            {
                result = globals.LState_Error.Clone();
                sbFieldNotPresent.AppendLine($"Field {model.ConfigToFieldLine.Name_Other} is not present in Textparser!");
            }

            if (!parserFields.Any(field => field.IdField == model.ConfigToFieldRoutine.ID_Other))
            {
                result = globals.LState_Error.Clone();
                sbFieldNotPresent.AppendLine($"Field {model.ConfigToFieldRoutine.Name_Other} is not present in Textparser!");
            }

            if (!parserFields.Any(field => field.IdField == model.ConfigToFieldRoutineType.ID_Other))
            {
                result = globals.LState_Error.Clone();
                sbFieldNotPresent.AppendLine($"Field {model.ConfigToFieldRoutineType.Name_Other} is not present in Textparser!");
            }

            if (!parserFields.Any(field => field.IdField == model.ConfigToFieldSchema.ID_Other))
            {
                result = globals.LState_Error.Clone();
                sbFieldNotPresent.AppendLine($"Field {model.ConfigToFieldSchema.Name_Other} is not present in Textparser!");
            }

            if (!parserFields.Any(field => field.IdField == model.ConfigToFieldServer.ID_Other))
            {
                result = globals.LState_Error.Clone();
                sbFieldNotPresent.AppendLine($"Field {model.ConfigToFieldServer.Name_Other} is not present in Textparser!");
            }

            if (result.GUID == globals.LState_Error.GUID)
            {
                result.Additional1 = "The following Fields are not present:\n" + sbFieldNotPresent.ToString();
            }

            return result;
        }

        public static clsOntologyItem ValidateExportToFileTextparser(TextParser textParser, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (textParser == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Textparser is not present!";
                return result;
            }

            if (string.IsNullOrEmpty( textParser.NameServer))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No Server present!";
                return result;
            }

            if (string.IsNullOrEmpty(textParser.NameIndexElasticSearch))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No Es-Index present!";
                return result;
            }

            if (string.IsNullOrEmpty(textParser.NameEsType))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No Es-Type present!";
                return result;
            }

            if (textParser.Port == 0)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No Port present!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateCheckExistanceDBItemsRequest(CheckExistanceDBItemsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is Empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is Invalid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetCheckExistanceDBItemsModel(CheckExistanceDBItemsModel model, OntologyModDBConnector dbReader,  Globals globals, string property)
        {
            var result = globals.LState_Success.Clone();
            if (property == nameof(CheckExistanceDBItemsModel.Config))
            {
                if (dbReader.Objects1.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one Config-Object, but you provided {dbReader.Objects1.Count} objects!";
                    return result;
                }

                model.Config = dbReader.Objects1.First();

                if (model.Config.GUID_Parent != CheckItemExistance.Config.LocalData.Class_Check_existance_of_Database_Items.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided Config-Object is not of Class {CheckItemExistance.Config.LocalData.Class_Check_existance_of_Database_Items.Name}";
                    return result;
                }
            }
            else if (property == nameof(CheckExistanceDBItemsModel.GetObjectsFromReportConfig))
            {
                if (dbReader.ObjectRels.Any())
                {
                    if (dbReader.ObjectRels.Count != 1)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = $"You need exact one Config for Objectlist from Report, but you provided {dbReader.ObjectRels.Count} objects!";
                        return result;
                    }

                    model.GetObjectsFromReportConfig = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).First();

                    if (model.GetObjectsFromReportConfig.GUID_Parent != CheckItemExistance.Config.LocalData.ClassRel_Check_existance_of_Database_Items_belonging_Source_Get_Objects_From_Report.ID_Class_Right)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = $"The provided Config for Objectlist from Report, is not of correct Class!";
                        return result;
                    }
                }   
            }
            else if (property == nameof(CheckExistanceDBItemsModel.DbItems))
            {
                model.DbItems = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                if ((!model.DbItems.Any() && model.GetObjectsFromReportConfig == null) ||
                    (model.DbItems.Any() && model.GetObjectsFromReportConfig != null))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You need a Objects from Report Config or attached Objects!";
                    return result;
                }

                var nonDbItems = model.DbItems.Where(item => item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Columns.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Jobs.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Linked_Servers.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Routines.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Synonyms.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Tables.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Triggers.GUID &&
                                                     item.GUID_Parent != CheckItemExistance.Config.LocalData.Class_DB_Views.GUID).ToList();
                if (nonDbItems.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The requested items are not all DB-Items:";
                    return result;
                }

                
            }

            return result;
        }

        public static clsOntologyItem ValidateGetMSSqlCodeRequest(GetMSSqlCodeRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID( request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid Id!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetGetMsSQLCodeModel(GetMsSQLCodeModel model, OntologyModDBConnector dbReader, Globals globals, string propertyName)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == nameof(GetMsSQLCodeModel.Config))
            {
                if (dbReader.Objects1.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Config not found!";
                    return result;
                }

                model.Config = dbReader.Objects1.First();
                if (model.Config.GUID_Parent != GetCodeOfDBItem.Config.LocalData.Class_Get_Code_of_DB_Item.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Config is not of class {GetCodeOfDBItem.Config.LocalData.Class_Get_Code_of_DB_Item.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(GetMsSQLCodeModel.DbItems))
            {
                model.DbItems = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();
            }

            return result;
        }
    }
}
