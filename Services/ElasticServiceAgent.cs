﻿using DatabaseManagementModule.Models;
using DatabaseManagementModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Services
{
    public class ElasticServiceAgent : ElasticBaseAgent
    {

        public async Task<ResultItem<List<MsSqlDbItemModel>>> GetMsSQLDbItemModel(GetColumnsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<MsSqlDbItemModel>>>(async () =>
            {
                var result = new ResultItem<List<MsSqlDbItemModel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<MsSqlDbItemModel>()
                };

                var searchDbItem = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdTableOrViewOrFunction
                   }
               };

                var dbConnector = new OntologyModDBConnector(globals);

                result.ResultState = dbConnector.GetDataObjects(searchDbItem);


                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Db-Item!";
                    return result;
                }

                var dbItem = dbConnector.Objects1.FirstOrDefault();

                if (dbItem == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"No Db-Item for {request.IdTableOrViewOrFunction} found!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateTableViewOrFunction(dbItem, globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result = await GetMsSQLDbItemModel(new List<clsOntologyItem> { dbItem });

                return result;
            });

            return taskResult;
        }
        

        public async Task<ResultItem<List<clsObjectRel>>> GetColumnsOfDbItem(List<string> dbItemIdList)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                result.ResultState = ValidationController.ValidateIdList(dbItemIdList, globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchDbColumns = dbItemIdList.Select(id => new clsObjectRel
                {
                    ID_Other = id,
                    ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID,
                    ID_Parent_Object = Config.LocalData.Class_DB_Columns.GUID
                }).ToList();

                var dbReaderDbColumns = new OntologyModDBConnector(globals);

                if (searchDbColumns.Any())
                {
                    result.ResultState = dbReaderDbColumns.GetDataObjectRel(searchDbColumns);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Db-Columns of Db-Items!";
                        return result;
                    }
                }

                result.Result = dbReaderDbColumns.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<MsSqlDbItemModel>>> GetMsSQLDbItemModel(List<clsOntologyItem> dbItemList)
        {
            var taskResult = await Task.Run<ResultItem<List<MsSqlDbItemModel>>>(() =>
           {
               var result = new ResultItem<List<MsSqlDbItemModel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<MsSqlDbItemModel>()
               };

               var searchSchema = dbItemList.Select(dbItem => new clsObjectRel
               {
                   ID_Object = dbItem.GUID,
                   ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID,
                   ID_Parent_Other = Config.LocalData.Class_Userschema.GUID
               }).ToList();

               var dbReaderSchema = new OntologyModDBConnector(globals);

               if (searchSchema.Any())
               {
                   result.ResultState = dbReaderSchema.GetDataObjectRel(searchSchema);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Userschema!";
                       return result;
                   }
               }

               result.Result = (from dbItem in dbItemList
                                join schema in dbReaderSchema.ObjectRels on dbItem.GUID equals schema.ID_Object
                                select new MsSqlDbItemModel
                                {
                                    DbItem = dbItem,
                                    DbSchema = new clsOntologyItem
                                    {
                                        GUID = schema.ID_Other,
                                        Name = schema.Name_Other,
                                        GUID_Parent = schema.ID_Parent_Other,
                                        Type = schema.Ontology
                                    }
                                }).ToList();

               return result;
           });

            return taskResult;
        }
        public async Task<ResultItem<DatabaseConnectionModel>> GetDatabaseConnectionModel(GetColumnsRequest getColumnsRequest)
        {
            var taskResult = await Task.Run<ResultItem<DatabaseConnectionModel>>(async () =>
           {
               var result = new ResultItem<DatabaseConnectionModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new DatabaseConnectionModel()
               };

               result.ResultState = ValidationController.ValidateGetColumnsRequest(getColumnsRequest, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchDbItem = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = getColumnsRequest.IdTableOrViewOrFunction
                   }
               };

               var dbConnector = new OntologyModDBConnector(globals);

               result.ResultState = dbConnector.GetDataObjects(searchDbItem);


               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Db-Item!";
                   return result;
               }

               var dbItem = dbConnector.Objects1.FirstOrDefault();

               if (dbItem == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = $"No Db-Item for {getColumnsRequest.IdTableOrViewOrFunction} found!";
                   return result;
               }

               result.ResultState = ValidationController.ValidateTableViewOrFunction(dbItem, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchDatabaseOnServer = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Other = dbItem.GUID,
                       ID_RelationType = Config.LocalData.RelationType_contains.GUID,
                       ID_Parent_Object = Config.LocalData.Class_Database_on_Server.GUID
                   }
               };

               result.ResultState = dbConnector.GetDataObjectRel(searchDatabaseOnServer);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the database on server";
                   return result;
               }

               var databasesOnServers = dbConnector.ObjectRels;
               if (getColumnsRequest.ConnectionOrderId != null)
               {
                   databasesOnServers = databasesOnServers.Where(dbOnServer => dbOnServer.OrderID == getColumnsRequest.ConnectionOrderId).ToList();
               }
               var databaseOnServer = dbConnector.ObjectRels.OrderBy(rel => rel.OrderID).Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Object,
                   Name = rel.Name_Object,
                   GUID_Parent = rel.ID_Parent_Object,
                   Type = globals.Type_Object
               }).FirstOrDefault();

               if (databaseOnServer == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Database on Server connected!";
                   return result;
               }

               var requestDatabaseConnectionModel = new GetDatabaseConnectionModelRequest(new List<clsOntologyItem>
               {
                   databaseOnServer
               });
               result = await GetDatabaseConnectionModel(requestDatabaseConnectionModel);

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<DatabaseConnectionModel>> GetDatabaseConnectionModel(GetDatabaseConnectionModelRequest request)
        {
            var taskResult = await Task.Run<ResultItem<DatabaseConnectionModel>>(() =>
           {
               var result = new ResultItem<DatabaseConnectionModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new DatabaseConnectionModel()
               };

               result.Result.DatabasesOnServers = request.DatabasesOnServers;
               var searchDatabase = result.Result.DatabasesOnServers.Select(dbOnServer => new clsObjectRel
               {
                   ID_Object = dbOnServer.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Database_on_Server_belongs_to_Database.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Database_on_Server_belongs_to_Database.ID_Class_Right
               }).ToList();

               var dbReaderDatabases = new OntologyModDBConnector(globals);

               if (searchDatabase.Any())
               {
                   result.ResultState = dbReaderDatabases.GetDataObjectRel(searchDatabase);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while geteting the Databases!";
                       return result;
                   }
               }

               result.Result.DbOnServersToDatabases = dbReaderDatabases.ObjectRels;

               var searchServer = result.Result.DatabasesOnServers.Select(dbOnServer => new clsObjectRel
               {
                   ID_Object = dbOnServer.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Database_on_Server_located_in_Server.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Database_on_Server_located_in_Server.ID_Class_Right
               }).ToList();

               var dbReaderServer = new OntologyModDBConnector(globals);

               if (searchServer.Any())
               {
                   result.ResultState = dbReaderServer.GetDataObjectRel(searchServer);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Servers!";
                       return result;
                   }

               }
               result.Result.DbOnServersToServers = dbReaderServer.ObjectRels;


               var searchInstance = result.Result.DatabasesOnServers.Select(dbOnInstance => new clsObjectRel
               {
                   ID_Object = dbOnInstance.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Database_on_Server_needs_Database_Instance.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Database_on_Server_needs_Database_Instance.ID_Class_Right
               }).ToList();

               var dbReaderInstance = new OntologyModDBConnector(globals);

               if (searchInstance.Any())
               {
                   result.ResultState = dbReaderInstance.GetDataObjectRel(searchInstance);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Instances!";
                       return result;
                   }

               }
               result.Result.DbOnServersToInstances = dbReaderInstance.ObjectRels;

               var searchDbAuthenatication = result.Result.DatabasesOnServers.Select(dbOnDbAuthenatication => new clsObjectRel
               {
                   ID_Object = dbOnDbAuthenatication.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Database_on_Server_authorized_by_DB_Authentication.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Database_on_Server_authorized_by_DB_Authentication.ID_Class_Right
               }).ToList();

               var dbReaderDbAuthenatication = new OntologyModDBConnector(globals);

               if (searchDbAuthenatication.Any())
               {
                   result.ResultState = dbReaderDbAuthenatication.GetDataObjectRel(searchDbAuthenatication);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the DbAuthenatications!";
                       return result;
                   }

               }
               result.Result.DbOnServersToDbAuthentications = dbReaderDbAuthenatication.ObjectRels;

               var searchUsers = result.Result.DbOnServersToDbAuthentications.Select(authentication => new clsObjectRel
               {
                   ID_Object = authentication.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_DB_Authentication_authorized_by_user.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_DB_Authentication_authorized_by_user.ID_Class_Right
               }).ToList();

               var dbReaderUsers = new OntologyModDBConnector(globals);

               if (searchUsers.Any())
               {
                   result.ResultState = dbReaderUsers.GetDataObjectRel(searchUsers);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Users!";
                       return result;
                   }

               }
               result.Result.DbAuthenticationsToUsers = dbReaderUsers.ObjectRels;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<CheckExistanceDBItemsModel>> GetCheckExistanceDBItemsModel(CheckExistanceDBItemsRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<CheckExistanceDBItemsModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new CheckExistanceDBItemsModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Config!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetCheckExistanceDBItemsModel(result.Result, dbReaderConfig, globals, nameof(CheckExistanceDBItemsModel.Config));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchConfigToObjectsFromReportConfig = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = CheckItemExistance.Config.LocalData.ClassRel_Check_existance_of_Database_Items_belonging_Source_Get_Objects_From_Report.ID_RelationType,
                        ID_Parent_Other = CheckItemExistance.Config.LocalData.ClassRel_Check_existance_of_Database_Items_belonging_Source_Get_Objects_From_Report.ID_Class_Right
                    }
                };

                var dbReaderConfigToObjectsFromReportConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigToObjectsFromReportConfig.GetDataObjectRel(searchConfigToObjectsFromReportConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Objects from Rport Config!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetCheckExistanceDBItemsModel(result.Result, dbReaderConfigToObjectsFromReportConfig, globals, nameof(CheckExistanceDBItemsModel.GetObjectsFromReportConfig));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<DBItemsWithListModel>> GetDBItemsWithListModel(List<clsOntologyItem> dbItems)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<DBItemsWithListModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new DBItemsWithListModel()
                };

                result.Result.DbItems = dbItems;

                var dbItemsNoSynonyms = dbItems.Where(dbItem => dbItem.GUID_Parent != Config.LocalData.Class_DB_Synonyms.GUID).ToList();
                var dbItemsSynonyms = dbItems.Where(dbItem => dbItem.GUID_Parent == Config.LocalData.Class_DB_Synonyms.GUID).ToList();

                var searchDbOnServers = dbItemsNoSynonyms.Select(dbItem => new clsObjectRel
                {
                    ID_Other = dbItem.GUID,
                    ID_RelationType = Config.LocalData.RelationType_contains.GUID,
                    ID_Parent_Object = Config.LocalData.Class_Database_on_Server.GUID
                }).ToList();

                var dbReaderDbOnServers = new OntologyModDBConnector(globals);

                if (searchDbOnServers.Any())
                {
                    result.ResultState = dbReaderDbOnServers.GetDataObjectRel(searchDbOnServers);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Database On Server relations of non-Synonyms!";
                        return result;
                    }
                }

                result.Result.DbOnServersToDBItems = dbReaderDbOnServers.ObjectRels;

                var searchDBSynonymsToDBOnServers = dbItemsSynonyms.Select(dbItem => new clsObjectRel
                {
                    ID_Object = dbItem.GUID,
                    ID_RelationType = CheckItemExistance.Config.LocalData.ClassRel_DB_Synonyms_located_at_Database_on_Server.ID_RelationType,
                    ID_Parent_Other = CheckItemExistance.Config.LocalData.ClassRel_DB_Synonyms_located_at_Database_on_Server.ID_Class_Right
                }).ToList();

                var dbReaderSynonyms = new OntologyModDBConnector(globals);

                if (searchDBSynonymsToDBOnServers.Any())
                {
                    result.ResultState = dbReaderSynonyms.GetDataObjectRel(searchDBSynonymsToDBOnServers);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Database On Server relations of Synonyms!";
                        return result;
                    }
                }

                result.Result.DBSynonymsToDBOnServers = dbReaderSynonyms.ObjectRels;

                var searchSchemaItems = result.Result.DbItems.Select(dbItem => new clsObjectRel
                {
                    ID_Object = dbItem.GUID,
                    ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID,
                    ID_Parent_Other = Config.LocalData.Class_Userschema.GUID
                }).ToList();

                var dbReaderSchema = new OntologyModDBConnector(globals);

                if (searchSchemaItems.Any())
                {
                    result.ResultState = dbReaderSchema.GetDataObjectRel(searchSchemaItems);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Schema-Items of DB-Items!";
                        return result;
                    }
                }

                result.Result.DbItemsToShemaItem = dbReaderSchema.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetMsSQLCodeModel>> GetMsSQLCodeModel(GetMSSqlCodeRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<GetMsSQLCodeModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetMsSQLCodeModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Config!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetGetMsSQLCodeModel(result.Result, dbReaderConfig, globals, nameof(result.Result.Config));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchDbItems = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = GetCodeOfDBItem.Config.LocalData.ClassRel_Get_Code_of_DB_Item_DB_Item.ID_RelationType
                    }
                };

                var dbReaderDbItems = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderDbItems.GetDataObjectRel(searchDbItems);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the DB-Items!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetGetMsSQLCodeModel(result.Result, dbReaderDbItems, globals, nameof(result.Result.DbItems));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ExportCodeToFileModel>> GetExportCodeToFileModel(ExportCodeToFileRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<ExportCodeToFileModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ExportCodeToFileModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Config!";
                    return result;
                }

                result.Result.Config = dbReaderConfig.Objects1.FirstOrDefault();

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderConfig, result.Result, globals, nameof(ExportCodeToFileModel.Config));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchFields = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_Parent_Other = ExportRoutine.Config.LocalData.Class_Field.GUID
                    }
                };

                var dbReaderFields = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderFields.GetDataObjectRel(searchFields);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Fields!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderFields, result.Result, globals, nameof(ExportCodeToFileModel.ConfigToFieldDatabase));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderFields, result.Result, globals, nameof(ExportCodeToFileModel.ConfigToFieldLine));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderFields, result.Result, globals, nameof(ExportCodeToFileModel.ConfigToFieldRoutine));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderFields, result.Result, globals, nameof(ExportCodeToFileModel.ConfigToFieldRoutineType));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderFields, result.Result, globals, nameof(ExportCodeToFileModel.ConfigToFieldSchema));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderFields, result.Result, globals, nameof(ExportCodeToFileModel.ConfigToFieldServer));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderFields, result.Result, globals, nameof(ExportCodeToFileModel.ConfigToFieldText));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchTextParser = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_belonging_Source_Textparser.ID_RelationType,
                        ID_Parent_Other = ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_belonging_Source_Textparser.ID_Class_Right
                    }
                };

                var dbReaderTextParser = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTextParser.GetDataObjectRel(searchTextParser);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the TextParser!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderTextParser, result.Result, globals, nameof(ExportCodeToFileModel.ConfigToTextparser));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchPattern = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_filtered_by_Pattern.ID_RelationType,
                        ID_Parent_Other = ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_filtered_by_Pattern.ID_Class_Right
                    }
                };

                var dbReaderPattern = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderPattern.GetDataObjectRel(searchPattern);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Pattern!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderPattern, result.Result, globals, nameof(ExportCodeToFileModel.ConfigToPattern));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchExportPath = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_export_to_Path.ID_RelationType,
                        ID_Parent_Other = ExportRoutine.Config.LocalData.ClassRel_Export_Code_to_File_export_to_Path.ID_Class_Right
                    }
                };

                var dbReaderExportPath = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderExportPath.GetDataObjectRel(searchExportPath);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Export-Path!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderExportPath, result.Result, globals, nameof(ExportCodeToFileModel.ConfigToPath));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchPatternPattern = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = result.Result.ConfigToPattern.ID_Other,
                        ID_AttributeType = ExportRoutine.Config.LocalData.ClassAtt_Pattern_Pattern.ID_AttributeType
                    }
                };

                var dbReaderPatternPattern = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderPatternPattern.GetDataObjectAtt(searchPatternPattern);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while geteting the Pattern-Attribute!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndPartlySetExportCodeToFileModel(dbReaderPatternPattern, result.Result, globals, nameof(ExportCodeToFileModel.Pattern));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public ElasticServiceAgent(Globals globals): base(globals)
        {
        }
    }
}
