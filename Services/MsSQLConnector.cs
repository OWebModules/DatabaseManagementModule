﻿using DatabaseManagementModule.Converter;
using DatabaseManagementModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Services
{
    public class MsSQLConnector
    {
        private Globals globals;
        
        public async Task<clsOntologyItem> CheckExistance(clsOntologyItem dbItem, string connectionString)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = globals.LState_Success.Clone();

                var objectTypes = DbItemClassToObjectTypeConverter.Convert(dbItem.GUID_Parent);
                if (!objectTypes.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Object-Type of DB-Item is not defined!";
                    return result;
                }
                var objectTypesInString = string.Join(", ", objectTypes.Select(ot => $"'{ot}'"));
                var sqlTemplate = Properties.Settings.Default.SQLCheckExistance;
                var query = sqlTemplate.Replace("@TYPES@", objectTypesInString).Replace("@NAME@", dbItem.Name);

                try
                {
                    var connection = new SqlConnection(connectionString);
                    connection.Open();
                    var command = new SqlCommand(query, connection);
                    var sqlReader = command.ExecuteReader();

                    var found = false;
                    while (sqlReader.Read())
                    {
                        found = true;
                        break;
                    }

                    if (!found)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "DB-Item was not found!";
                    }

                    sqlReader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<string>> GetMsCode(MsSqlDbItemModel dbItemModel, string connectionString)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<string>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                try
                {
                    var sbCode = new StringBuilder();
                    var connection = new SqlConnection(connectionString);
                    connection.Open();
                    var query = string.Format(Properties.Settings.Default.SQLGetItemCode, dbItemModel.DbSchema.Name, dbItemModel.DbItem.Name);
                    var command = new SqlCommand(query, connection);
                    var sqlReader = command.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        sbCode.AppendLine(sqlReader.GetString(0));
                    }

                    sqlReader.Close();
                    connection.Close();

                    result.Result = sbCode.ToString();
                }
                catch (Exception ex)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = ex.Message;
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetMsColumnsResult>> GetMsColumns(MsSqlDbItemModel dbItemModel, string connectionString)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<GetMsColumnsResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetMsColumnsResult
                    {
                        DbItem = dbItemModel.DbItem,
                        ColumnRawItems = new List<ColumnRawItem>()
                    }
                };

                try
                {
                    var connection = new SqlConnection(connectionString);
                    connection.Open();
                    var query = string.Format(Properties.Settings.Default.SQLGetColumns, dbItemModel.DbSchema.Name, dbItemModel.DbItem.Name);
                    var command = new SqlCommand(query, connection);
                    var sqlReader = command.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        var columnInfo = new ColumnRawItem
                        {
                            Column = new clsOntologyItem
                            {
                                Name = sqlReader["name"].ToString(),
                                GUID_Parent = Config.LocalData.Class_DB_Columns.GUID,
                                Type = globals.Type_Object
                            },
                            DataType = new clsOntologyItem
                            {
                                Name = sqlReader["name_type"].ToString(),
                                GUID_Parent = Config.LocalData.Class_DataTypes__Ms_SQL_.GUID,
                                Type = globals.Type_Object
                            },
                            MaxLength = (int)sqlReader["max_length"]
                        };
                        result.Result.ColumnRawItems.Add(columnInfo);
                    }

                    sqlReader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = ex.Message;
                    return result;
                }
                
                return result;
            });

            return taskResult;
        }

        public MsSQLConnector(Globals globals)
        {
            this.globals = globals;
        }
    }
}
