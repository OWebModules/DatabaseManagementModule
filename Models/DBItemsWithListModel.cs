﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class DBItemsWithListModel
    {
        public List<clsOntologyItem> DbItems { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> DbItemsToShemaItem { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> DbOnServersToDBItems { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> DBSynonymsToDBOnServers { get; set; } = new List<clsObjectRel>();

        public List<clsOntologyItem> ValidDBItemsWithConnectionInfo { get; set; } = new List<clsOntologyItem>();
    }
}
