﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class CheckExistanceDBItemsWithListRequest
    {
        public List<clsOntologyItem> DBItems { get; private set; } = new List<clsOntologyItem>();

        public bool QueryDBItems { get; set; } = false;
        public CancellationToken CancellationToken { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public string MasterPassword { get; private set; }

        public CheckExistanceDBItemsWithListRequest(List<clsOntologyItem> dbItemIds, string masterPassword, CancellationToken cancellationToken)
        {
            DBItems = dbItemIds;
            MasterPassword = masterPassword;
            CancellationToken = cancellationToken;
        }
    }
}
