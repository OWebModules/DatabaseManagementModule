﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class CheckConnectionStringRequest
    {
        public string IdConnectionString { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public CheckConnectionStringRequest(string idConnectionString)
        {
            IdConnectionString = idConnectionString;
        }
    }
}
