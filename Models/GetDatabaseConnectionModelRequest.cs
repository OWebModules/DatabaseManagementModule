﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class GetDatabaseConnectionModelRequest
    {
        public List<clsOntologyItem> DatabasesOnServers { get; private set; } = new List<clsOntologyItem>();

        public GetDatabaseConnectionModelRequest(List<clsOntologyItem> databasesOnServers)
        {
            DatabasesOnServers = databasesOnServers;
        }
    }
}
