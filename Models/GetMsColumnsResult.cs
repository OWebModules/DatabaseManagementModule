﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class GetMsColumnsResult
    {
        public clsOntologyItem DbItem { get; set; }
        public List<ColumnRawItem> ColumnRawItems { get; set; } = new List<ColumnRawItem>();
    }

    public class ColumnRawItem
    {
        public clsOntologyItem Column { get; set; }
        public clsOntologyItem DataType { get; set; }
        public long MaxLength { get; set; }
    }
}
