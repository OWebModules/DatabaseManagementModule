﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class ExportCodeToFileResult
    {
        public clsOntologyItem Config { get; set; }
        public clsOntologyItem SessionItem { get; set; }
        public List<clsObjectAtt> SessionPaths { get; set; } = new List<clsObjectAtt>();
    }
}
