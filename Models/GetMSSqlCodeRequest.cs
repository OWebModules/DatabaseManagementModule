﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class GetMSSqlCodeRequest
    {
        public string IdConfig { get; private set; }

        public string MasterPassword { get; private set; }
        public CancellationToken CancellationToken { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public GetMSSqlCodeRequest(string idConfig, string masterPassword, CancellationToken cancellationToken)
        {
            IdConfig = idConfig;
            MasterPassword = masterPassword;
            CancellationToken = cancellationToken;
        }
    }
}
