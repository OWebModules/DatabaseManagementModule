﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class MsSqlDbItemModel
    {
        public clsOntologyItem DbItem { get; set; }
        public clsOntologyItem DbSchema { get; set; }
    }
}
