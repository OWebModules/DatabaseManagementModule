﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using SecurityModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class DatabaseConnectionModel
    {
        public List<clsOntologyItem> DatabasesOnServers { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> DbOnServersToDatabases { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> DbOnServersToServers { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> DbOnServersToInstances { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> DbOnServersToDbAuthentications { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> DbAuthenticationsToUsers { get; set; } = new List<clsObjectRel>();

        public async Task<ResultItem<List<clsOntologyItem>>> GetConnectionStringsMsSql(Globals globals, string masterPassword)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async () =>
              {
                  var result = new ResultItem<List<clsOntologyItem>>
                  {
                      ResultState = globals.LState_Success.Clone(),
                      Result = DatabasesOnServers
                  };

                  var securityController = new SecurityController(globals);

                  var authenticationInfos = (from authentication in DbOnServersToDbAuthentications
                                             join userItem in DbAuthenticationsToUsers on authentication.ID_Other equals userItem.ID_Object
                                             select new { authentication, userItm = new clsOntologyItem { GUID = userItem.ID_Other, Name = userItem.Name_Other, GUID_Parent = userItem.ID_Parent_Other, Type = userItem.Ontology } });
                  var connectionInfos = (from dbOnServer in DatabasesOnServers
                                         join database in DbOnServersToDatabases on dbOnServer.GUID equals database.ID_Object
                                         join server in DbOnServersToServers on dbOnServer.GUID equals server.ID_Object
                                         join instance in DbOnServersToInstances on dbOnServer.GUID equals instance.ID_Object into instances1
                                         from instance in instances1.DefaultIfEmpty()
                                         join authentication in authenticationInfos on dbOnServer.GUID equals authentication.authentication.ID_Object into authentications
                                         from authentication in authentications.DefaultIfEmpty()
                                         select new { dbOnServer, database, server, instance, authentication });

                  var authenticationString = "Integrated Security=SSPI";
                  foreach (var dbOnServer in connectionInfos)
                  {
                      var instance = dbOnServer.instance != null ? $@"\{dbOnServer.instance.Name_Other}" : "";
                      if (dbOnServer.authentication != null)
                      {
                          var resultCredentials = await securityController.GetPassword(dbOnServer.authentication.userItm, masterPassword);
                          result.ResultState = resultCredentials.Result;
                          if (result.ResultState.GUID == globals.LState_Error.GUID)
                          {
                              return result;
                          }

                          var credentialItem = resultCredentials.CredentialItems.FirstOrDefault();
                          if (credentialItem == null)
                          {
                              result.ResultState = globals.LState_Error.Clone();
                              result.ResultState.Additional1 = $"The Database on Server {dbOnServer.dbOnServer.Name} is not configured correctly, no credentials!";
                              return result;
                          }

                          var user = dbOnServer.authentication.userItm.Name;
                          var password = credentialItem.Password.Name_Other;

                          authenticationString = $"User Id={user};Password={password};";
                      }
                      var connectionString = $@"Data Source={ dbOnServer.server.Name_Other }{instance};Initial Catalog={dbOnServer.database.Name_Other};{authenticationString}";
                      dbOnServer.dbOnServer.Additional1 = connectionString;
                  }

                  return result;
              });

            return taskResult;

        }
    }
}
