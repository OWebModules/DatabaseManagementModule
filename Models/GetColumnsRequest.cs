﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class GetColumnsRequest
    {
        public string IdTableOrViewOrFunction { get; private set; }
        public int? ConnectionOrderId { get; set; }
        public string MasterPassword { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public GetColumnsRequest(string idTableOrViewOrFunction, string masterPassword)
        {
            IdTableOrViewOrFunction = idTableOrViewOrFunction;
            MasterPassword = masterPassword;
        }
    }
}
