﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class GetMsSQLCodeModel
    {
        public clsOntologyItem Config { get; set; }
        public List<clsOntologyItem> DbItems { get; set; }
    }
}
