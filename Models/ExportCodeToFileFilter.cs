﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class ExportCodeToFileFilter
    {
        public ExportCodeToFileModel Model { get; set; }
        public string Server { get; set; }
        public string Database { get; set; }
        public string Schema { get; set; }
        public string RoutineType { get; set; }
        public string Routine { get; set; }
        public string FileName { get; set; }
        public string EsServer { get; set; }
        public int EsPort { get; set; }
        public string EsIndex { get; set; }
        public string EsType { get; set; }

        public List<SortField> SortFields { get; set; }

        public string GetQuery()
        {
            var sbQuery = new StringBuilder();
            sbQuery.Append($"{Model.ConfigToFieldServer.Name_Other}:\"{Server}\"");
            sbQuery.Append($" AND {Model.ConfigToFieldDatabase.Name_Other}:\"{Database}\"");
            sbQuery.Append($" AND {Model.ConfigToFieldSchema.Name_Other}:\"{Schema}\"");
            sbQuery.Append($" AND {Model.ConfigToFieldRoutineType.Name_Other}:\"{RoutineType}\"");
            sbQuery.Append($" AND {Model.ConfigToFieldRoutine.Name_Other}:\"{Routine}\"");

            return sbQuery.ToString();
        }


    }
}
