﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class CheckExistanceDBItemsRequest
    {
        public string IdConfig { get; private set; }
        public CancellationToken CancellationToken { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public string MasterPassword { get; private set; }

        public CheckExistanceDBItemsRequest(string idConfig, string masterPassword, CancellationToken cancellationToken)
        {
            IdConfig = idConfig;
            MasterPassword = masterPassword;
            CancellationToken = cancellationToken;
        }
    }
}
