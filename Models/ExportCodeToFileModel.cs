﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Models
{
    public class ExportCodeToFileModel
    {
        public clsOntologyItem Config { get; set; }
        public clsObjectRel ConfigToFieldRoutineType { get; set; }
        public clsObjectRel ConfigToFieldSchema { get; set; }
        public clsObjectRel ConfigToFieldServer { get; set; }
        public clsObjectRel ConfigToFieldDatabase { get; set; }
        public clsObjectRel ConfigToFieldLine { get; set; }
        public clsObjectRel ConfigToFieldRoutine { get; set; }
        public clsObjectRel ConfigToFieldText { get; set; }
        public clsObjectRel ConfigToPath { get; set; }

        public clsObjectRel ConfigToPattern { get; set; }

        public clsObjectAtt Pattern { get; set; }
        public clsObjectRel ConfigToTextparser { get; set; }
    }
}
