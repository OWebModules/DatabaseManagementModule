﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManagementModule.Converter
{
    public static class DbItemClassToObjectTypeConverter
    {
        public static List<string> Convert(string idClassDbItemType)
        {
            if (idClassDbItemType == CheckItemExistance.Config.LocalData.Class_DB_Routines.GUID)
            {
                return new List<string> { "P", "PC", "RF", "X", "AF", "FN", "FS", "FT", "IF", "TF" };
            }
            if (idClassDbItemType == CheckItemExistance.Config.LocalData.Class_DB_Tables.GUID)
            {
                return new List<string> { "U" };
            }
            if (idClassDbItemType == CheckItemExistance.Config.LocalData.Class_DB_Triggers.GUID)
            {
                return new List<string> { "TA", "TR" };
            }
            if (idClassDbItemType == CheckItemExistance.Config.LocalData.Class_DB_Synonyms.GUID)
            {
                return new List<string> { "S", "SN" };
            }
            if (idClassDbItemType == CheckItemExistance.Config.LocalData.Class_DB_Views.GUID)
            {
                return new List<string> { "V" };
            }

            return new List<string>();
        }
    }
}
